gw2-enemycount
======

## About
This is a tool for Guild Wars 2's WvW mode to count the enemies and show what classes they're using. This tool also replicates the WvW fight summary produced with the Aleeva discord bot.

## Setup & Usage
### Dependencies

#### Guild Wars 2
1. https://account.arena.net/welcome OR https://store.steampowered.com/app/1284210/Guild_Wars_2/

#### arcdps
There are various ways to obtain and correctly install arcdps. My preferred way is through the Guild Wars 2 Addon Manager:
1. https://github.com/gw2-addon-loader/GW2-Addon-Manager/wiki

#### PlenBotLogUploader
1. https://github.com/Plenyx/PlenBotLogUploader

### This Tool
1. Download the [latest exe](https://gitlab.com/api/v4/projects/rigel314%2Fgw2-enemycount/jobs/artifacts/master/raw/gw2-enemycount.exe?job=build)
   * Or the [windows console version](https://gitlab.com/api/v4/projects/rigel314%2Fgw2-enemycount/jobs/artifacts/master/raw/gw2-enemycount-console.exe?job=build)
     * This version attaches to a cmd window to display log messages
   * Or the [linux version](https://gitlab.com/api/v4/projects/rigel314%2Fgw2-enemycount/jobs/artifacts/master/raw/gw2-enemycount?job=build)
2. Optionally, set configuration in enemycount.json next to the exe - see [enemycount-example.json](enemycount-example.json)
   * You can also provide your own stylesheet by creating main.css next to the exe - see [main.css](www/main.css)
3. Double click the exe
   * This tool has no GUI, other than the webpage it hosts
4. Browse to http://127.0.0.1/ or if you changed the `HTTPport` setting (eg 8080) http://127.0.0.1:8080/
   * This tool binds to all interfaces by default, but you can set the `HTTPbind` setting to an IP to specify a single interface to bind to
5. Configure a remote server ping in PlenBotLogUploader
   * Method needs to be POST
   * URL is your browser URL with `/ping/` appended, eg http://127.0.0.1/ping/ or http://127.0.0.1:8080/ping/
   * You can hit the "test ping" button, which really just makes sure the url and method are correct. It won't add any fight data to the web page.

## Why
I play the WvW mode of Guild Wars 2. There's a DPS meter called arcdps which supports a plugin architecture. There's an arcdps plugin called KnowThyEnemy, which tells you how many player enemies you fought and what classes they were playing. Apparently, some part of the way KnowThyEnemy and arcdps communicate is not stable, so any time arcdps updates, it breaks KnowThyEnemy. It takes a little while for the KnowThyEnemy developer to update and parts of KnowThyEnemy are closed source (presumably due to an agreement with the company behind Guild Wars 2). Even when the plugin is not working, if arcdps is working, the arcdps logs can be used to find this information anyway, it's just less convenient. One of my guild mates is configured to automatically post a summary of the arcdps logs in a discord channel, where we can easily see the enemy count. But to see the class breakdown, you must click on the link in the discord summary to show the full fight details in a browser. Sometimes this guild member is not playing with us, so we can't do that either - additionally, this means we don't get the rest of the Aleeva discord bot fight summary.

This project aims to replace KnowThyEnemy, without being an arcdps plugin, and to give a fight summary similar to the Aleeva discord bot.

## How it works
This tool is intended to run as a local webapp. When you run it, it runs a local web server and when loaded in a browser, will display any fight data it knows about. As new fight data becomes available, through PlenBotLogUploader pings, the webapp will display that fight data at the top of the page.

This uses a bit of a round-about way of getting the information I wanted for two reasons:
* I didn't want to parse the arcdps logs myself or run GW2 Elite Insights Parser locally
* I wanted to have the arcdps logs uploaded to dps.report

This means this tool works by being an extra step after a normal arcdps log upload workflow. If you use PlenBotLogUploader, you can configure it to send a "ping" to this tool.

```mermaid
sequenceDiagram
	participant gw2
	participant arcdps
	participant plenbot uploader
	participant dps.report
	participant gw2-enemycount
	participant browser

	note over browser: page load
	browser ->> gw2-enemycount: get fights
	gw2-enemycount -->> browser: known fights
	note over gw2,arcdps: battle
	arcdps ->> plenbot uploader: log detect
	plenbot uploader ->> dps.report: log upload
	dps.report -->> plenbot uploader: upload response
	plenbot uploader ->> gw2-enemycount: ping
	gw2-enemycount ->> dps.report: /getJson api query
	dps.report -->> gw2-enemycount: /getJson response
	gw2-enemycount -->> plenbot uploader: ping response
	gw2-enemycount ->> browser: websocket packet
	note over browser: display fight data
```

## Debugging
* The PlenBotLogUploader ping format is specified on [their wiki](https://github.com/Plenyx/PlenBotLogUploader/blob/main/remote-server/README.md)
* There are some comments on the Elite Insights JSON format in [their source code](https://github.com/baaron4/GW2-Elite-Insights-Parser/tree/master/GW2EIJSON)
* You can test sending fight data to gw2-enemycount by sending the same kind of http request
  * I've been using `curl -d "permalink=https://dps.report/w.report/GNtK-20240508-195850_wvw" -X POST http://192.168.0.167/ping/`
* You can set `"CreateLogfile": true` in the config file to append to a log in the working directory called gw2-enemycount.log
  * This is just the stuff that would be printed to the console (stderr) in the windows console version
* You can set `"ExitHang": true` in the config file on the windows console version to keep the console window open until the user hits enter

## Known Bugs
* Enemy summary down count seems weird, since it's frequently lower than death count, but I'm doing the same thing for processing the friend summary down count which looks correct - so maybe they're both wrong or maybe it's wrong to process them both the same...
* Right now, we can only tell friend from foe, we can't tell which team the enemies are on, so if you're in a 3-way in WvW, this tool will show all the enemies in one list
  * I'm pretty sure this is information that arcdps normally provides, but isn't providing at this time
