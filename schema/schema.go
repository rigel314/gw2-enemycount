package schema

// this file holds all the structs that correspond to the json output from dps.report

import (
	"log"
	"time"
)

// Schema is the "top level" object
type Schema struct {
	TimeEndStd           Time     `json:"timeEndStd"`
	FightName            string   `json:"fightName"`
	Targets              []Target `json:"targets"`
	Players              []Player `json:"players"`
	Phases               []Phase  `json:"phases"`
	RecordedBy           string   `json:"recordedBy"`
	EliteInsightsVersion string   `json:"eliteInsightsVersion"`
	ArcVersion           string   `json:"arcVersion"`
	ArcRevision          int      `json:"arcRevision"`
	GW2Build             int      `json:"gW2Build"`
	Duration             string   `json:"duration"`
}

// Target is a single enemy
type Target struct {
	Name         string     `json:"name"`
	EnemyPlayer  bool       `json:"enemyPlayer"`
	IsFake       bool       `json:"isFake"`
	DpsAllPhases []DpsAll   `json:"dpsAll"`
	Defenses     []Defenses `json:"defenses"`
	TeamID       int        `json:"teamID"`
}

// DpsAll holds the damage stats for an entity
type DpsAll struct {
	Dps    int `json:"dps"`
	Damage int `json:"damage"`
}

// Defenses holds the stats related to defense for an entity
type Defenses struct {
	DownCount int `json:"downCount"`
	DeadCount int `json:"deadCount"`
}

// Player is a single friend
type Player struct {
	NotInSquad      bool            `json:"notInSquad"`
	Profession      string          `json:"profession"`
	FriendlyNPC     bool            `json:"friendlyNPC"`
	Name            string          `json:"name"`
	IsFake          bool            `json:"isFake"`
	DpsTargets      [][]DpsTarget   `json:"dpsTargets"`
	StatsTargets    [][]StatsTarget `json:"statsTargets"`
	SupportPhases   []Support       `json:"support"`
	Defenses        []Defenses      `json:"defenses"`
	TeamID          int             `json:"teamID"`
	GuildID         string          `json:"guildID"`
	HasCommanderTag bool            `json:"hasCommanderTag"`
	ExtHealingStats struct {
		OutgoingHealingAllies [][]HealingTarget `json:"outgoingHealingAllies"`
	} `json:"extHealingStats"`
	ExtBarrierStats struct {
		OutgoingBarrierAllies [][]BarrierTarget `json:"outgoingBarrierAllies"`
	} `json:"extBarrierStats"`
}

// DpsTarget holds damage stats a player did against a target
type DpsTarget struct {
	Dps    int `json:"dps"`
	Damage int `json:"damage"`
}

type StatsTarget struct {
	Killed int `json:"killed"`
	Downed int `json:"downed"`
}

// Support holds support stats for a player
type Support struct {
	CondiCleanse     int `json:"condiCleanse"`
	CondiCleanseSelf int `json:"condiCleanseSelf"`
	BoonStrips       int `json:"boonStrips"`
}

// HealingTarget holds healing stats for a player
type HealingTarget struct {
	Hps     int `json:"hps"`
	Healing int `json:"healing"`
}

// BarrierTarget holds barrier stats for a player
type BarrierTarget struct {
	Bps     int `json:"hps"`
	Barrier int `json:"barrier"`
}

type Phase struct {
	Name string `json:"name"`
}

// Time wraps a time.Time and defines a custom JSON unmarshal function to decode the time string in the right way
type Time struct {
	time.Time
}

// UnmarshalJSON decodes a time string from the timeEndStd field in the json from dps.report
func (t *Time) UnmarshalJSON(b []byte) error {
	// golang's time parsing strings are neat, instead of something like %Y-%M-%D, you just have to
	// use the fixed strings that represent a datetime component in a particular format eg year is
	// always 2006, but might be in the format string as 06 for a 2-digit year
	date, err := time.Parse(`"2006-01-02 15:04:05 -07:00"`, string(b))
	if err != nil {
		// if we failed parsing the date, that doesn't need to prevent parsing of the entire rest of the report
		log.Println("failed parsing timeEndStd, using time.Now() instead:", err)
		t.Time = time.Now()
		return nil
	}
	t.Time = date
	return nil
}
