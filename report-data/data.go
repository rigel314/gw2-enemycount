package data

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"path"
	"slices"
	"sort"
	"strings"

	"gitlab.com/rigel314/gw2-enemycount/schema"
)

func DataFromUrl(url string) (*Data, error) {
	return DefaultDataFetcher().DataFromUrl(url)
}

type GuildIDs interface {
	Lookup(id string) (guild Gw2apiGuild, ok bool, resolve bool)
	Resolved(id string, guild Gw2apiGuild)
}

// DataFetcher is public so you can manipulate the public members, you should always call DefaultDataFetcher() to get the initial instance
type DataFetcher struct {
	BaseDomains  map[string]string
	GuildIDs     GuildIDs
	Logger       log.Logger
	guildIDCache map[string]Gw2apiGuild
}

func DefaultDataFetcher() *DataFetcher {
	return &DataFetcher{
		BaseDomains: map[string]string{
			"dps.report":   "https://dps.report/getJson?permalink=",
			"b.dps.report": "https://b.dps.report/getJson?permalink=",
		},
	}
}

func (r *DataFetcher) DataFromUrl(raw_url string) (*Data, error) {
	u, err := url.Parse(raw_url)
	if err != nil {
		return nil, fmt.Errorf("failed to parse url: %w", err)
	}

	if r.BaseDomains == nil {
		return nil, fmt.Errorf("BUG: nil BaseDomains")
	}
	base_url, ok := r.BaseDomains[u.Hostname()]
	if !ok {
		return nil, fmt.Errorf("hostname %q not in BaseDomains, skipping", u.Hostname())
	}

	// get the json from the url
	getresp, err := http.Get(base_url + path.Base(raw_url))
	if err != nil {
		return nil, fmt.Errorf("failure fetching dps report: %w", err)
	}
	defer getresp.Body.Close()

	// complain if the http response was unsuccessful
	if getresp.StatusCode != 200 {
		return nil, fmt.Errorf("failure fetching dps report, non 200 status code: %q", getresp.Status)
	}

	// decode the json from dps.report
	jdec := json.NewDecoder(getresp.Body)
	var report schema.Schema
	err = jdec.Decode(&report)
	if err != nil {
		return nil, fmt.Errorf("failure decoding dps report: %w", err)
	}

	// find the main "phase" for fights that have multiple phases - it seems to make a new phase for players with a breakbar, maybe if they're holding a dragon banner or something?
	mainPhase := -1
	for i, v := range report.Phases {
		if v.Name == "Detailed Full Fight" {
			mainPhase = i
		}
	}
	if mainPhase == -1 {
		return nil, fmt.Errorf("missing phase named 'Detailed Full Fight'")
	}

	// create a new data object for the summary of this report
	d := &Data{
		UnixTimestamp:        report.TimeEndStd.Unix(),
		FightName:            report.FightName,
		URL:                  raw_url,
		RecordedBy:           report.RecordedBy,
		EliteInsightsVersion: report.EliteInsightsVersion,
		ArcVersion:           report.ArcVersion,
		ArcRevision:          report.ArcRevision,
		GW2Build:             report.GW2Build,
		Duration:             report.Duration,
	}

	teamIDs := make(map[int]int)
	for _, v := range report.Targets {
		// don't count enemy NPCs or "fake" enemies
		if v.EnemyPlayer && !v.IsFake {
			teamIDs[v.TeamID]++
		}
	}
	if len(teamIDs) == 0 {
		teamIDs[0] = 1
	}
	classTeams := make(map[int]map[string]int, len(teamIDs))
	d.EnemyTeamsSummary = make(map[int]*Summary)
	for k := range teamIDs {
		classTeams[k] = make(map[string]int)
		d.EnemyTeamsSummary[k] = &Summary{}
	}

	// loop through enemies
	for _, v := range report.Targets {
		// don't count enemy NPCs or "fake" enemies
		if v.EnemyPlayer && !v.IsFake {
			d.EnemySummary.TeamCount++
			d.EnemyTeamsSummary[v.TeamID].TeamCount++

			// TODO: make sure the assumption holds that enemies are named "class ignored-gibberish"
			class := strings.Split(v.Name, " ")
			if len(class) == 2 {
				classTeams[v.TeamID][class[0]]++ //increment this class count
			}

			// don't look at this enemy's Defenses here, only look later when iterating players so you can only count deaths/downs caused by squad members

			if len(v.DpsAllPhases) > mainPhase {
				stat := v.DpsAllPhases[mainPhase]
				d.EnemySummary.Damage += stat.Damage                // add to their Damage summary
				d.EnemySummary.DPS += stat.Dps                      // add to their Dps summary
				d.EnemyTeamsSummary[v.TeamID].Damage += stat.Damage // add specific team Damage summary
				d.EnemyTeamsSummary[v.TeamID].DPS += stat.Dps       // add specific team Dps summary
			}

		}
	}

	// turn the map of classes into an array of classCount
	d.EnemyClassCounts = make(map[int][]ClassCount)
	for tid, classes := range classTeams {
		for k, v := range classes {
			d.EnemyClassCounts[tid] = append(d.EnemyClassCounts[tid], ClassCount{
				Class: k,
				Count: v,
			})
		}
	}

	for tid := range d.EnemyClassCounts {
		// TODO: do these sorts in one function

		// sort the array by class name
		sort.SliceStable(d.EnemyClassCounts[tid], func(i, j int) bool {
			return d.EnemyClassCounts[tid][i].Class < d.EnemyClassCounts[tid][j].Class
		})
		// sort the array by count, this second sort wins, but is stable, so classes with the same count will be ordered alphabetically
		sort.SliceStable(d.EnemyClassCounts[tid], func(i, j int) bool {
			return d.EnemyClassCounts[tid][i].Count > d.EnemyClassCounts[tid][j].Count
		})
	}

	stats := make([]FriendStats, 0, len(report.Players))
	// loop through friends
	for playerIdx, v := range report.Players {
		// don't count friendly NPCs or "fake" players
		if !v.FriendlyNPC && !v.IsFake {
			if v.NotInSquad {
				d.NotInSquad++ // increment not in squad count
			} else {
				d.FriendSummary.TeamCount++ // increment friend count - not in squad shouldn't count
			}

			s := FriendStats{}
			s.Name = v.Name        // assign Name
			s.Class = v.Profession // assign Profession

			s.Guild, err = r.guildName(v.GuildID)
			if err != nil {
				r.Logger.Println("ignoring guildName lookup error: " + err.Error())
			}

			// loop through the double array of damage against targets
			for j, phases := range v.DpsTargets {
				if len(phases) > mainPhase {
					damage := phases[mainPhase]
					// TODO: make sure the j number actually consistently has the same length as and corresponds to the target list

					// only consider damage stats against targets who are not NPCs or "fake" targets
					if j < len(report.Targets) && report.Targets[j].EnemyPlayer && !report.Targets[j].IsFake {
						s.Dps += damage.Dps       // add to Dps
						s.Damage += damage.Damage // add to Damage

						d.FriendSummary.DPS += damage.Dps       // add to Dps summary
						d.FriendSummary.Damage += damage.Damage // add to Damage summary
					}
				}
			}
			if len(v.SupportPhases) > mainPhase {
				sup := v.SupportPhases[mainPhase]
				s.Strips += sup.BoonStrips              // add to Strips
				s.CleansesTotal += sup.CondiCleanse     // add to Cleanses
				s.CleansesTotal += sup.CondiCleanseSelf // add to Cleanses
				s.CleansesOutgoing += sup.CondiCleanse
			}
			for healingPlayerIdx, phases := range v.ExtHealingStats.OutgoingHealingAllies {
				if len(phases) > mainPhase {
					healing := phases[mainPhase]
					s.HealingTotal += healing.Healing // add to Healing
					if healingPlayerIdx != playerIdx {
						s.HealingOutgoing += healing.Healing
					}
				}
			}
			for barrierPlayerIdx, phases := range v.ExtBarrierStats.OutgoingBarrierAllies {
				if len(phases) > mainPhase {
					barrier := phases[mainPhase]
					s.BarrierTotal += barrier.Barrier // add to Barrier
					if barrierPlayerIdx != playerIdx {
						s.BarrierOutgoing += barrier.Barrier
					}
				}
			}

			// count downs/deaths but don't count downs/deaths of non squad members or enemy downs/deaths caused exclusively by non squad members
			if !v.NotInSquad {
				if len(v.Defenses) > mainPhase {
					stat := v.Defenses[mainPhase]
					d.FriendSummary.Deaths += stat.DeadCount // add our Deaths
					d.FriendSummary.Downs += stat.DownCount  // add our Downs
				}
				if len(v.StatsTargets) == len(report.Targets) {
					for i, v := range v.StatsTargets {
						if len(v) > mainPhase {
							stat := v[mainPhase]
							if report.Targets[i].EnemyPlayer && !report.Targets[i].IsFake && len(report.Targets[i].Defenses) > mainPhase {
								d.EnemySummary.Deaths += stat.Killed                                // add to their Deaths
								d.EnemySummary.Downs += stat.Downed                                 // add to their Downs
								d.EnemyTeamsSummary[report.Targets[i].TeamID].Deaths += stat.Killed // add specific team Deaths
								d.EnemyTeamsSummary[report.Targets[i].TeamID].Downs += stat.Downed  // add specific team Downs
							}
						}
					}
				}
			}

			if v.HasCommanderTag {
				d.Commander = &s
			}

			// append this player to the list of friends
			stats = append(stats, s)
		}
	}

	// duplicate and sort the list of friends for the different top10 lists
	d.Top10.Damage = make([]FriendStats, len(stats))
	d.Top10.Cleanses = make([]FriendStats, len(stats))
	d.Top10.Strips = make([]FriendStats, len(stats))
	d.Top10.Healing = make([]FriendStats, len(stats))
	d.Top10.Barrier = make([]FriendStats, len(stats))
	copy(d.Top10.Damage, stats)
	copy(d.Top10.Cleanses, stats)
	copy(d.Top10.Strips, stats)
	copy(d.Top10.Healing, stats)
	copy(d.Top10.Barrier, stats)
	slices.SortFunc(d.Top10.Damage, func(a, b FriendStats) int { return b.Damage - a.Damage })
	slices.SortFunc(d.Top10.Cleanses, func(a, b FriendStats) int { return b.CleansesTotal - a.CleansesTotal })
	slices.SortFunc(d.Top10.Strips, func(a, b FriendStats) int { return b.Strips - a.Strips })
	slices.SortFunc(d.Top10.Healing, func(a, b FriendStats) int { return b.HealingTotal - a.HealingTotal })
	slices.SortFunc(d.Top10.Barrier, func(a, b FriendStats) int { return b.BarrierTotal - a.BarrierTotal })

	return d, nil
}

var unknownGuild = Gw2apiGuild{
	Tag:  "???",
	Name: "???",
}

func (r *DataFetcher) guildName(id string) (*Gw2apiGuild, error) {
	if id == "" || id == "00000000-0000-0000-0000-000000000000" {
		return nil, nil
	}

	if r.guildIDCache == nil {
		r.guildIDCache = make(map[string]Gw2apiGuild)
	}

	if v, ok := r.guildIDCache[id]; ok {
		return &v, nil
	}

	if r.GuildIDs == nil {
		return nil, nil
	}

	g, ok, resolve := r.GuildIDs.Lookup(id)
	if ok {
		r.guildIDCache[id] = g
		return &g, nil
	}

	if !resolve {
		return &unknownGuild, nil
	}

	getresp, err := http.Get("https://api.guildwars2.com/v2/guild/" + id + "?v=2021-07-15T13:00:00.002Z")
	if err != nil {
		return &unknownGuild, fmt.Errorf("failure fetching guild info: %w", err)
	}
	defer getresp.Body.Close()

	// complain if the http response was unsuccessful
	if getresp.StatusCode != 200 {
		return &unknownGuild, fmt.Errorf("failure fetching guild info, non 200 status code: %q", getresp.Status)
	}

	// decode the json
	jdec := json.NewDecoder(getresp.Body)
	var guild Gw2apiGuild
	err = jdec.Decode(&guild)
	if err != nil {
		return &unknownGuild, fmt.Errorf("failure decoding guild info: %w", err)
	}

	r.guildIDCache[id] = guild

	r.GuildIDs.Resolved(id, guild)

	return &guild, nil
}

type Gw2apiGuild struct {
	Level          int    `json:"level"`
	MOTD           string `json:"motd"`
	Influence      int    `json:"influence"`
	Aetherium      int    `json:"aetherium"`
	Favor          int    `json:"favor"`
	MemberCount    int    `json:"member_count"`
	MemberCapacity int    `json:"member_capacity"`
	ID             string `json:"id"`
	Name           string `json:"name"`
	Tag            string `json:"tag"`
	Emblem         struct {
		Background struct {
			ID     int   `json:"id"`
			Colors []int `json:"colors"`
		} `json:"background"`
		Foreground struct {
			ID     int   `json:"id"`
			Colors []int `json:"colors"`
		} `json:"foreground"`
	} `json:"emblem"`
	Flags []string `json:"flags"`
}
