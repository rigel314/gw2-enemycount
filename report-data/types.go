package data

// Data is the summarized data after parsing the dps.report output
type Data struct {
	NotInSquad           int
	EnemySummary         Summary
	EnemyTeamsSummary    map[int]*Summary
	FriendSummary        Summary
	EnemyClassCounts     map[int][]ClassCount
	Top10                Top10
	UnixTimestamp        int64
	FightName            string
	URL                  string
	RecordedBy           string
	EliteInsightsVersion string
	ArcVersion           string
	ArcRevision          int
	GW2Build             int
	Commander            *FriendStats
	Duration             string
}

// ClassCount is the count of enemies playing a particular class
type ClassCount struct {
	Count int
	Class string
}

// Summary holds some overall stats for the group of friends or group of enemies
type Summary struct {
	Damage    int
	DPS       int
	Downs     int
	Deaths    int
	TeamCount int
}

// Top10 holds the sorted top10 lists for each top10 display
type Top10 struct {
	Damage   []FriendStats
	Cleanses []FriendStats
	Strips   []FriendStats
	Healing  []FriendStats
	Barrier  []FriendStats
}

// FriendStats contains stats for a particular friend
type FriendStats struct {
	Name             string
	Class            string
	Guild            *Gw2apiGuild
	Dps              int
	Damage           int
	CleansesTotal    int
	CleansesOutgoing int
	Strips           int
	HealingTotal     int
	HealingOutgoing  int
	BarrierTotal     int
	BarrierOutgoing  int
}
