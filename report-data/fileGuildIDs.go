package data

import (
	"encoding/json"
	"os"
)

const curVersion = 1

type fileGuildIDs struct {
	L       map[string]Gw2apiGuild `json:"lookup"`
	Version int                    `json:"version"`
	file    *os.File               `json:"-"`
}

func NewFileGuildIDs(path string) *fileGuildIDs {
	file, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return nil
	}
	f := &fileGuildIDs{
		L:    make(map[string]Gw2apiGuild),
		file: file,
	}

	json.NewDecoder(file).Decode(f)

	if f.Version != curVersion {
		f.L = make(map[string]Gw2apiGuild)
	}

	return f
}

func (f *fileGuildIDs) Lookup(id string) (guild Gw2apiGuild, ok bool, resolve bool) {
	v, ok := f.L[id]
	return v, ok, true
}

func (f *fileGuildIDs) Resolved(id string, guild Gw2apiGuild) {
	f.L[id] = guild
	f.file.Seek(0, 0)
	f.file.Truncate(0)
	jenc := json.NewEncoder(f.file)
	jenc.SetIndent("", "\t")
	jenc.Encode(f)
	f.file.Sync()
}

var _ GuildIDs = &fileGuildIDs{}
