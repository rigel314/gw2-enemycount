package data

// Request holds the input data
type Request struct {
	Permalink  string `json:"permalink"`
	BossId     int    `json:"bossId"`
	Success    bool   `json:"success"`
	ArcVersion string `json:"arcVersion"`
}

// Response holds the output message
type Response struct {
	Msg string `json:"msg"`
}
