package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime/debug"
	"slices"

	"github.com/gorilla/websocket"
	data "gitlab.com/rigel314/gw2-enemycount/report-data"
)

// root returns a handler function for handling the URI: '/'
func (s *server) root() http.HandlerFunc {
	// process the index template
	tmpl, err := assets.ReadFile("www/index.html.tmpl")
	if err != nil {
		// if the template is missing, this is a programming bug, so panic
		panic("missing index template")
	}
	tmpltxt := string(tmpl)
	// actually parse the template, using Must because if this fails, that's a programming bug, so panic
	t := template.Must(template.New("page").Funcs(template.FuncMap{
		"inc": func(x int) int {
			return x + 1
		},
		"divint": func(f string, x, y int) string {
			return fmt.Sprintf(f, float64(x)/float64(y))
		},
		"divintnorm": func(f string, x, nx, y, ny int) string {
			return fmt.Sprintf(f, (float64(x)/float64(nx))/(float64(y)/float64(ny)))
		},
		"addint": func(x, y int) int {
			return x + y
		},
	}).Parse(tmpltxt))

	return func(w http.ResponseWriter, r *http.Request) {
		// create a copy of the server data, to avoid a race condition with log processing
		svr := s.Copy()
		// reverse this copy, so newest stuff is first
		slices.Reverse(svr.Data)
		// apply the template, outputting directly to the ResponseWriter
		err := t.Execute(w, svr)
		if err != nil {
			log.Println(err)
		}
	}
}

// css returns a handler function for serving the main.css file either from the embedded default or from the filesystem next to the executable
func (s *server) css() http.HandlerFunc {
	// get the path to the running exe, if it fails just use the current working directory
	exepath, err := os.Executable()
	var exedir string
	if err == nil {
		exedir = filepath.Dir(exepath)
	} else {
		exedir = "."
	}

	// build a path to main.css next to the exe
	csspath := filepath.Join(exedir, "main.css")

	return func(w http.ResponseWriter, r *http.Request) {
		// try to open the main.css file
		f, err := os.Open(csspath)
		if err != nil {
			// if it doesn't work, just hand off to the default
			http.FileServerFS(assets).ServeHTTP(w, r)
			return
		}
		// defer the file close
		defer f.Close()
		// set the content type so browsers will actually render the css
		// this was obnoxious, because it was loading fine, just not rendering
		w.Header().Set("Content-Type", "text/css; charset=utf-8")
		// start dumping the contents of the file from disk to the ResponseWriter
		_, err = io.Copy(w, f)
		if err != nil {
			log.Println(err)
		}
	}
}

// notify acts as the server side of a websocket and any connected clients will get a notification whenever a new log is processed
func (s *server) notify() http.HandlerFunc {
	// buffer sizes for the websockets
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	// response holds the websocket notification message
	type response struct {
		Status string
	}
	return func(w http.ResponseWriter, r *http.Request) {
		// directly upgrade this connection to a websocket
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}
		defer conn.Close()

		// loop, waiting for log events
		for {
			// TODO: fix potential DOS attack because this stays blocked on the condition variable even if the client disconnects

			// condition variables can only be waited on while locked, so lock
			s.notifyer.L.Lock()
			// wait for the notification
			s.notifyer.Wait()
			// immediately unlock the condition because we don't actually need the lock.
			s.notifyer.L.Unlock()

			// send a websocket message
			err := conn.WriteJSON(response{Status: "refresh"})
			if err != nil {
				// don't log this error because it's probably the client disconnecting or refreshing the webpage, which happens on purpose
				break
			}
		}
	}
}

// ping generates the summary when given a permalink to dps.report
func (s *server) ping() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// default the output message to "success"
		resp := data.Response{Msg: "success"}
		jenc := json.NewEncoder(w)
		// defer a function to encode the response into the ResponseWriter
		defer func() {
			err := jenc.Encode(resp)
			if err != nil {
				log.Println(err)
			}
		}()

		// defer a function to handle a panic, and update the response message
		defer func() {
			if r := recover(); r != nil {
				resp.Msg = fmt.Sprintf("failure, unexpected panic: %v", r)
				log.Println(resp.Msg + "\n" + string(debug.Stack())) // no need to serve the full stack trace
			}
		}()

		// assign request from the parsed inputs
		var req data.Request
		jdec := json.NewDecoder(r.Body)
		err := jdec.Decode(&req)
		if err != nil {
			resp.Msg = "failure reading request body: " + err.Error()
			log.Println(resp.Msg)
			return
		}

		log.Printf("%+v\n", req)

		d, err := s.dataFetcher.DataFromUrl(req.Permalink)
		if err != nil {
			resp.Msg = "failure decoding dps report: " + err.Error()
			log.Println(resp.Msg)
			return
		}

		// append this report summary to the list of reports
		s.Data = append(s.Data, d)
		// notify any connected websockets
		s.notifyer.Broadcast()

		// var aoeub bytes.Buffer
		// aoeugenc := gob.NewEncoder(&aoeub)
		// aoeugenc.Encode(d)
		// log.Printf("%+v\n", base64.RawStdEncoding.EncodeToString((aoeub.Bytes())))
	}
}
