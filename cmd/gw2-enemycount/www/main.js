// called on startup
$(function () {
	// start updating any timestamp fields
	periodicUpdateTimes()

	// connect the websocket, and setup for reconnect
	reconnect(true)

	// check for updates
	checkForUpdate()

	// fill in team colors
	teamColors()

	// register click handlers for changing teamID->color mapping
	registerColorChangeClicks()
})

// getColorMap returns a mapping of teamID to color, with a default mapping if none currently exists
function getColorMap() {
	// load and parse color map
	let colorMap_s = localStorage.getItem("colorMap")
	if (colorMap_s == null) {
		colorMap_s = "{\"432\": {\"color\": \"Blue\", \"timestamp\": 0}, \"705\": {\"color\": \"Red\", \"timestamp\": 0}, \"2752\": {\"color\": \"Green\", \"timestamp\": 0}}"
	}
	return JSON.parse(colorMap_s)
}

// teamColors fills in the team colors based on the teamID anywhere that matters
function teamColors() {
	let colorMap = getColorMap()

	// for each team color item, fill in the color name from the mapping or just put the team id there if the lookup fails
	$("span.teamID_color").each(function (idx, e) {
		let elem = $(e)
		let parent = elem.parent()

		// get the team id
		let id = parent.data("teamid")

		if (colorMap[id]) {
			elem.text(colorMap[id].color)
		} else {
			elem.text(id)
		}
	})
}

// withinLastReset returns true iff timestamp more recent than the most recent WvW reset.
function withinLastReset(timestamp) {
	let reset = new Date()
	let ts = new Date(timestamp)

	// calculate last reset time
	while (reset.getUTCDay() != 6) {
		// subtract one day at a time until we get to saturday
		reset.setUTCDate(reset.getUTCDate() - 1)
	}
	// 2am
	reset.setUTCHours(2)
	reset.setUTCMinutes(0)
	reset.setUTCSeconds(0)
	reset.setUTCMilliseconds(0)

	if (ts > reset) {
		return true
	}

	return false
}

// registerColorChangeClicks registers click handlers on the svgs to allow changing the color map
function registerColorChangeClicks() {
	$("span.teamID>svg").on("mouseenter", function () {
		$(this).children("circle.r").attr("cy", 6.732)
		$(this).children("circle.g").attr("cy", 6.732)
		$(this).children("circle.b").attr("cy", 2.5)
	}).on("mouseleave", function () {
		$(this).children("circle.r").attr("cy", 2.5)
		$(this).children("circle.g").attr("cy", 2.5)
		$(this).children("circle.b").attr("cy", 6.732)
	})
	$("span.teamID>svg").on("click", function () {
		let elem = $(this)
		let parent = elem.parent()

		// add r, g, and b buttons to choose a color for this team id
		// removes buttons if they're already shown
		// there should only be one of these children
		parent.children("span.teamID_select").each(function (idx, e) {
			let elem = $(e)
			if (elem.children().length == 0) {
				let rbutton = $("<button class='team_id_choose team_id_choose_r'>r</button>")
				rbutton.on("click", buttonClickHandler(parent.data("teamid"), "Red", parent))
				let gbutton = $("<button class='team_id_choose team_id_choose_g'>g</button>")
				gbutton.on("click", buttonClickHandler(parent.data("teamid"), "Green", parent))
				let bbutton = $("<button class='team_id_choose team_id_choose_b'>b</button>")
				bbutton.on("click", buttonClickHandler(parent.data("teamid"), "Blue", parent))
				elem.empty()
				elem.append(rbutton)
				elem.append(gbutton)
				elem.append(bbutton)
			} else {
				elem.empty()
			}
		})
	})
}

function buttonClickHandler(id, color, data) {
	return function () {
		let colorMap = getColorMap()

		// update color for this id
		colorMap[id] = { color: color, timestamp: Date.now() }

		// save color map
		localStorage.setItem("colorMap", JSON.stringify(colorMap))

		// delete rgb buttons
		data.children("span.teamID_select").each(function (idx, e) {
			let elem = $(e)
			elem.empty()
		})

		// re-apply team colors
		teamColors()
	}
}

// updateTimes finds all timestamps and fill them in with the amount of elapsed time since that timestamp
function updateTimes() {
	// get now in seconds since unix epoch
	let now = Math.floor(Date.now() / 1000)

	// loop through all timestamps
	$("span.timestamp").each(function (i, e) {
		e = $(e) // jQuery-ify this object
		let fighttime = e.attr("data-timestamp") // get the fight timestamp from the served attribute
		let diff = now - fighttime // calculate the difference in seconds
		let ago = "s ago"
		if (diff > 60) { // more than 60 seconds? turn it into minutes
			diff = Math.round(diff / 60)
			ago = "m ago"
			if (diff > 60) { // more than 60 minutes? turn it into hours
				diff = Math.round(diff / 60)
				ago = "h ago"
				if (diff > 24) { // more than 24 hours? turn it into days, I don't care about years or anything bigger
					diff = Math.round(diff / 24)
					ago = "d ago"
				}
			}
		}
		e.text(diff + ago)
	})
}

// periodicUpdateTimes calls updateTimes on a loop every second or so
function periodicUpdateTimes() {
	updateTimes()
	setTimeout(periodicUpdateTimes, 1000)
}

// reconnect connects to the notify websocket, and attempts a reconnect once per second or so on disconnect
function reconnect(first) {
	// get url information to handle different ports, http(s), and reverse proxies adding a subdirectory
	let proto = window.location.protocol
	let host = window.location.host
	let uri = window.location.pathname

	let wsproto = "ws"
	if (proto == "https:") {
		wsproto = "wss"
	}

	try {
		// build the websocket url and attempt to connect
		let socket = new WebSocket(wsproto + "://" + host + uri + "api/fight-notify");
		if (!first) { // if this was not the initial reconnect(true) call
			// setup to reload the page on open, because that means we reconnected the server was restarted
			socket.addEventListener("open", (event) => {
				window.location.reload()
			});
		}
		// setup to handle incoming messages to reload the page, because that means there's new fight data
		socket.addEventListener("message", (event) => {
			window.location.reload()
		});
		// setup to handle close by attempting to reconnect(false)
		socket.addEventListener("close", (event) => {
			reconnect(false)
		});
	} catch {
		// if anything above fails, just try again in a second or so
		setTimeout(reconnect, 1000, false)
	}
}

function checkForUpdate() {
	let currentVersion = $("#version").text()
	let checkUrl = $("#version").attr("data-update-url")
	$.getJSON(checkUrl)
		.done(function (data) {
			if (!data || !Array.isArray(data) || data.length < 1 || !data[0] || !data[0].sha) {
				console.log("invalid response from update check url")
				return
			}
			if (data[0].sha != currentVersion) {
				let uaspan = $("#update-available")
				let a = $("<a></a>")
				a.attr("href", uaspan.attr("data-download-url"))
				a.text("Update available")
				uaspan.empty()
				uaspan.append(" ")
				uaspan.append(a)
			}
		})
		.fail(function () {
			console.log("failed to check for update")
		})
}
