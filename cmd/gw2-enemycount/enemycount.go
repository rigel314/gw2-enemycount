package main

import (
	"bytes"
	"context"
	"embed"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"runtime/debug"
	"strings"
	"sync"
	"time"

	"github.com/shirou/gopsutil/v3/process"
	data "gitlab.com/rigel314/gw2-enemycount/report-data"
	"golang.org/x/term"
)

// cfg holds describes the schema for the config file and holds the values from the config file
type cfg struct {
	HTTPport      int
	HTTPbind      string
	ExitWithGW2   bool
	CreateLogfile bool
	ExitHang      bool
}

// server represents the shared context for the pages and api being served
type server struct {
	Cfg         cfg
	Version     string
	Data        []*data.Data
	notifyer    *sync.Cond
	notifyMtx   *sync.Mutex
	ctx         context.Context
	cancel      context.CancelFunc
	dataFetcher *data.DataFetcher
}

// startupBuffer is used as an extra logger destination until the config file is read and we decide
// whether to log to a file, in which case this buffer is dumped to the file, then discarded.
// if we're not logging to a file, the buffer is just discarded
var startupBuffer bytes.Buffer

// main is the entry point
func main() {
	// print the file and line number along with the normal log messages
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// log to both stderr and the startupBuffer
	log.SetOutput(io.MultiWriter(os.Stderr, &startupBuffer))

	// create a server instance
	s := &server{
		Cfg: cfg{
			ExitHang: true,
		},
	}
	// defer exitHang incase something panics, we'll be able to log the panic output to the screen
	// and optional logfile and optionally wait for the user to hit "enter" before exiting
	defer s.exitHang()

	// create a context which is closed when this process gets an interrupt or kill signal
	ctx, _ := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	// wrap that context in another one which is cancelable via function call
	s.ctx, s.cancel = context.WithCancel(ctx)

	// read the version of this executable from the buildinfo, or just default to the string "Uncontrolled"
	gitVer := "Uncontrolled"
	if bi, ok := debug.ReadBuildInfo(); ok {
		for _, kv := range bi.Settings {
			if kv.Key == "vcs.revision" {
				gitVer = kv.Value
			}
		}
	}
	log.Println("Version:", gitVer)

	// initialize the rest of the fields of the server instance
	s.Version = gitVer
	s.notifyMtx = &sync.Mutex{}
	s.notifyer = sync.NewCond(s.notifyMtx)

	s.dataFetcher = data.DefaultDataFetcher()
	fetcher := data.NewFileGuildIDs(".guildIDCache_gw2-enemycount.json")
	if fetcher != nil {
		s.dataFetcher.GuildIDs = fetcher
	}

	// load the config file
	s.LoadConfig()

	// run the actual web server in another goroutine
	go s.serve()
	if s.Cfg.ExitWithGW2 {
		// if we're supposed to exit with gw2, then also run the gw2 monitor in another goroutine
		go s.gw2runningCheck()
	}

	// block the main goroutine until the context is closed by signal or cancellation
	<-s.ctx.Done()

	// reaching the end of main stops all other goroutines
}

// server runs the web server, handling all the requests
func (s *server) serve() {
	// defer a function to make crashes actually get logged to the logfile before exiting the process
	defer func() {
		if v := recover(); v != nil {
			log.Println("stacktrace from panic:", v, "\n"+string(debug.Stack()))
		}
		// cancel the context the main thread is blocked on
		s.cancel()
	}()

	// register all the endpoints with the default serveMux
	http.Handle("GET /{$}", s.root())
	http.Handle("GET /www/", http.FileServerFS(assets))
	http.Handle("GET /www/main.css", s.css())
	http.Handle("GET /www/index.html.tmpl", http.NotFoundHandler())
	http.Handle("POST /ping/{$}", s.ping())
	http.Handle("/api/fight-notify", s.notify())

	// actually start the server
	log.Println("server up")
	log.Println(http.ListenAndServe(s.Cfg.HTTPbind+":"+fmt.Sprint(s.Cfg.HTTPport), nil))

	// the only way this should return is an error (like port already bound) or a panic (like index out of range)
}

// gw2runningCheck monitors the gw2 process to see if it transitions from existing to not existing - the flag to exit this program
func (s *server) gw2runningCheck() {
	// defer a function to make crashes actually get logged to the logfile before exiting the process
	defer func() {
		if v := recover(); v != nil {
			log.Println("stacktrace from panic:", v, "\n"+string(debug.Stack()))
		}
		// cancel the context the main thread is blocked on
		s.cancel()
	}()

	// ckstate is an "enum" that represents the state of this state machine
	type ckstate int
	const (
		waitForGW2start ckstate = iota
		waitForGW2exit  ckstate = iota
	)
	var state ckstate
	for {
		// check every 10sec or so
		time.Sleep(10 * time.Second)

		// list processes
		ps, err := process.Processes()
		if err != nil {
			log.Println("error listing processes:", err)
			continue
		}
		switch state {
		case waitForGW2start: // we haven't yet seen a running gw2 process
			// check for a gw2 process
			for _, v := range ps {
				name, err := v.Name()
				if err != nil {
					log.Println("error getting the name of process:", err)
					continue
				}
				name = strings.ToLower(name)
				if name == "gw2.exe" || name == "gw2-64.exe" {
					// we found it, cool, go to the "wait for exit" state
					log.Println("found gw2 process")
					state = waitForGW2exit
					break
				}
			}
		case waitForGW2exit: // we have seen a running gw2 process, wait until it's gone
			// check for the absence of a gw2 process
			found := false
			for _, v := range ps {
				name, err := v.Name()
				if err != nil {
					continue
				}
				name = strings.ToLower(name)
				if name == "gw2.exe" || name == "gw2-64.exe" {
					found = true
				}
			}
			if !found {
				// we didn't find it, time to exit
				log.Println("exiting because gw2 is not running")
				return // trigger the deferred function, which will cancel the main context
			}
		}
	}
}

//go:embed www/*
var assets embed.FS // the comment above makes the build embed the files in the www/ folder into the executable and accessible in this variable

//go:embed enemycount-example.json
var exampleCfg []byte // the comment above makes the build embed the file contents into this variable - this serves as the default config

// LoadConfig reads the config file from next to the exe or in your current working directory
func (s *server) LoadConfig() {
	// defcfg serves to hold the default config options
	var defcfg cfg
	// parse the default config from the committed example config
	err := json.Unmarshal(exampleCfg, &defcfg)
	if err != nil {
		// if the example config is unparsable, this is a programming bug, so it should panic
		panic(err)
	}

	// get a path to the running exe, and if it fails, default to "current directory"
	exepath, err := os.Executable()
	var exedir string
	if err == nil {
		exedir = filepath.Dir(exepath)
	} else {
		exedir = "."
	}

	cfgpath := filepath.Join(exedir, "enemycount.json")
	firsttry := cfgpath

	// assign the default config to the final storage place for the config options
	s.Cfg = defcfg

	// try opening the default path
	f, err := os.Open(cfgpath)
	if err != nil {
		// it didn't work, thy the fallback path in the current directory
		cfgpath = filepath.Join(".", "enemycount.json")
		f, err = os.Open(cfgpath)
		if err != nil {
			// it didn't work either, but not having a config file is okay, they'll just get the defaults
			log.Println("failed to open config file at", firsttry, "or", cfgpath, ", using defaults")
		}
	}
	if err == nil {
		// we opened a config file

		// defer closing the file
		defer f.Close()

		// parse the config from the file
		jdec := json.NewDecoder(f)
		err = jdec.Decode(&s.Cfg)
		if err != nil {
			// it failed to parse, oh well, use the defaults
			s.Cfg = defcfg
			log.Println("failed to decode config file, using defaults. File:", cfgpath)
		}
	}

	// if we're not creating a logfile, stop logging to the startupBuffer and return now
	if !s.Cfg.CreateLogfile {
		log.SetOutput(os.Stderr)
		startupBuffer = bytes.Buffer{} // discard startupBuffer contents
		return
	}

	// create a log file
	logpath := filepath.Join(".", "gw2-enemycount.log")
	flog, err := os.OpenFile(logpath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		// if it failed, we don't get a log file
		log.Println("failed opening", logpath, ": ", err)
		return
	}

	// start logging to the logfile instead of the startupBuffer
	log.SetOutput(io.MultiWriter(os.Stderr, flog))

	// write the contents of the startupBuffer to the logfile
	_, err = startupBuffer.WriteTo(flog)
	if err != nil {
		log.Println(err)
	}

	// discard startupBuffer contents
	startupBuffer = bytes.Buffer{}
}

// Copy creates a copy of the data portions of server for the webpage to avoid a race condition with a new log being processed
func (s *server) Copy() *server {
	var ret server
	ret.Version = s.Version
	ret.Cfg = s.Cfg
	ret.Data = make([]*data.Data, len(s.Data))
	copy(ret.Data, s.Data) // this is not a "deep" copy, but it's okay, the children objects shouldn't be being changed by the log processing
	return &ret
}

// hwindowsgui is a global variable only set true on windows builds that set -ldflags="-H=windowsgui", see ./hwindowsgui_windows.go
var hwindowsgui bool

// exitHang runs on exit from the main goroutine to keep the console window open until the user hits "enter"
// ExitHang must be specified in the config file and this must not be a windows gui-only exe
func (s *server) exitHang() {
	// check for a crash, if there is one, recover and print it normally
	if v := recover(); v != nil {
		log.Println("stacktrace from panic:", v, "\n"+string(debug.Stack()))
	}
	// make sure the user can see a console and wants to hang on exit
	if hwindowsgui || !s.Cfg.ExitHang {
		return
	}
	// wait for "enter" to be hit
	fmt.Println("hit enter to exit")
	_, err := term.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		log.Println(err)
	}
}
