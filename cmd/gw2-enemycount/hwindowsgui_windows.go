package main

// this file is only part of the build on windows because of the filename ending in _windows.go

import (
	"debug/pe"
	"os"
)

// init checks if this is a windows gui or windows cui program
// all init() functions run before main, and there can be more than one function called "init"
func init() {
	exepath, err := os.Executable()
	if err != nil {
		return
	}
	f, err := pe.Open(exepath)
	if err != nil {
		return
	}
	defer f.Close()
	var subsystem uint16
	if header, ok := f.OptionalHeader.(*pe.OptionalHeader64); ok {
		subsystem = header.Subsystem
	} else if header, ok := f.OptionalHeader.(*pe.OptionalHeader32); ok {
		subsystem = header.Subsystem
	}

	if subsystem == pe.IMAGE_SUBSYSTEM_WINDOWS_GUI {
		hwindowsgui = true
	}
}
